﻿using CADImport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DWGDemo
{
    public partial class Form1 : Form
    {
        string dwg = @"G0805-1.dwg";

        public Form1()
        {
            InitializeComponent();
        }


        private int GetTexts(CADEntityCollection entities)
        {
            int result = 0;
            for (int i = 0; i < entities.Count; i++)
            {
                if (entities[i].EntType == EntityType.Text)
                {
                    Console.WriteLine("EntName: {0}, EntLocation: {1}", entities[i].EntName, toString(entities[i].Box));
                    result++;
                }
                else if (entities[i].EntType == EntityType.Proxy)
                    result += GetTexts((entities[i] as CADProxy).Entities);
            }
            return result;
        }

        private string toString(DRect box)
        {
            return string.Format("{0}, {1} : {2}, {3}", box.TopLeft.X.ToString("0.00"), box.TopLeft.Y.ToString("0.00"),
                box.BottomRight.X.ToString("0.00"), box.BottomRight.Y.ToString("0.00"));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CADImage img = CADImage.CreateImageByExtension(dwg);
            img.LoadFromFile(dwg);

            int vCount = GetTexts(img.CurrentLayout.Entities);
            Console.WriteLine("There are " + vCount + " Text entities on the current layout of the drawing");

            addLine(img, 0, 0, 150, 150);
            addLine(img, 0, 10, 100, 150);
            addLine(img, 0, 0, 1000, 0);
            addLine(img, 0, 0, 0, 1000);

            img.Draw(pictureBox1.CreateGraphics(), new RectangleF(0, 0, pictureBox1.Width, pictureBox1.Height));// (float)img.AbsWidth, (float)img.AbsHeight));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CADImage img = new CADImage();
            img.InitialNewImage();

            addLine(img, 0, 0, 150, 150);
            addLine(img, 0, 10, 100, 150);
            addLine(img, 0, 0, 1000, 0);
            addLine(img, 0, 0, 0, 1000);

            img.Draw(pictureBox1.CreateGraphics(), new RectangleF(0, 0, pictureBox1.Width, pictureBox1.Height));


        }


        void addLine(CADImage img, int x1, int y1, int x2, int y2, double weight = 0.3)
        {
            CADLine line = new CADLine();
            line.Point = new DPoint(x1, y1, 0);
            line.Point1 = new DPoint(x2, y2, 0);
            line.Color = Color.Blue;
            line.LineWeight = weight;
            img.Converter.Loads(line);
            img.CurrentLayout.AddEntity(line);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            CADImage img = CADImage.CreateImageByExtension(dwg);
            img.LoadFromFile(dwg);

            button3.Text = String.Format("{0} x {1} x {2}", img.AbsWidth, img.AbsHeight, img.AbsDepth);
        }
    }
}
